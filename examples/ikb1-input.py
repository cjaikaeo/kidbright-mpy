# The following code demonstrates digital input reading using iKB-1 board.
# Assuming a switch board is attached to port 0, the code will display the
# switch status every 0.5 second.
import kidbright as kb
import ikb1
import time

kb.init()
ikb1.init()

sw = ikb1.Input(0)

while True:
    print("SW value =", sw.value())
    time.sleep(0.5)

