# The following code demonstrates output control using iKB-1 board.
# Assuming an LED board is attached to port 1, the code will blink the LED
# every second.
import kidbright as kb
import ikb1
import time

kb.init()
ikb1.init()

led = ikb1.Output(1)

while True:
    led.value(1)
    time.sleep(0.5)
    led.value(0)
    time.sleep(0.5)

