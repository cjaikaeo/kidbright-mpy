# The following code demonstrates DC motor control using iKB-1 board.
# Assuming a servo is connected to the servo port 15, the code will cause
# the servo to repeatedly swing back and forth.
import kidbright as kb
import ikb1
import time

kb.init()
ikb1.init()

s1 = ikb1.Servo(15)

while True:
    for angle in range(0,180,5):
        s1.position(angle)
        time.sleep(0.1)
    for angle in range(180,0,-5):
        s1.position(angle)
        time.sleep(0.1)
