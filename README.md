# KidBright's Micropython Library

This is a Micropython library for KidBright boards.  It provides access to 
on-board peripherals such as the LED matrix, switches, and sensors.

The module ikb1.py is also provided for accessing peripherals on the iKB-1
board.
