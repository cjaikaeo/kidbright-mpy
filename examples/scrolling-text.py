import time
import kidbright as kb

def scroll_text(text):
    for i in range(len(text)*8 + 16):
        kb.matrix.fill(0)
        kb.matrix.text(text, 16-i, 0)
        kb.matrix.show()
        time.sleep(0.1)

kb.init()
while True:
    scroll_text("Hello, World!")
