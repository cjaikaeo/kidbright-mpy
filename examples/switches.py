import time
import kidbright as kb

kb.init()

while True:
    kb.matrix.fill(0)
    if kb.sw1():
        kb.matrix.text("1", 0, 0)
    if kb.sw2():
        kb.matrix.text("2", 8, 0)
    kb.matrix.show()
    time.sleep(0.1)
