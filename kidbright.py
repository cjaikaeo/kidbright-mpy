from machine import Pin, I2C, ADC
import time
import framebuf

I2C0_SCL = const(22)
I2C0_SDA = const(21)
I2C1_SCL = const(5)
I2C1_SDA = const(4)

HT16K33_ADDR = const(0x70)
HT16K33_CMD_OSC = const(0x20)
HT16K33_CMD_DISP = const(0x80)
HT16K33_CMD_BRIGHTNESS = const(0xE0)

LDR_MAX_VALUE = const(3400)
LDR_ADC_PIN = const(36)
SW1_PIN = const(16)
SW2_PIN = const(14)

LM73_ADDR = const(0x4d)
LM73_REG_TEMPERATURE = const(0x00)
LM73_REG_CONFIG = const(0x01)
LM73_REG_THI = const(0x02)
LM73_REG_TLOW = const(0x03)
LM73_REG_CTRLSTATUS = const(0x04)
LM73_REG_ID = const(0x05)
LM73_POWER_OFF = const(0xc0)
LM73_POWER_ON = const(0x40)

_ldr = None
_sw1 = None
_sw2 = None
_i2c1 = None
_lm73_data = bytearray(2)

#######################
class matrix:
    _i2c = None
    _buf = bytearray(16)  # hold buffer for 16x8 LED matrix
    _bufx = bytearray(16) # hold buffer for rearranged _buf
    _fb = framebuf.FrameBuffer(_buf,16,8,framebuf.MONO_VMSB)

    @staticmethod
    def init():
        __class__._i2c = I2C(0,scl=Pin(I2C0_SCL),sda=Pin(I2C0_SDA))
        i2c = __class__._i2c
        i2c.writeto(HT16K33_ADDR,bytearray([HT16K33_CMD_OSC | 0x01]))
        i2c.writeto(HT16K33_ADDR,bytearray([HT16K33_CMD_BRIGHTNESS | 0x07]))
        i2c.writeto(HT16K33_ADDR,bytearray([HT16K33_CMD_DISP | 0x01]))
        matrix.fill(0)
        matrix.show()

    def show():
        buf = __class__._buf
        bufx = __class__._bufx
        i2c = __class__._i2c
        for i in range(8):
            bufx[i*2] = buf[i]
            bufx[i*2+1] = buf[i+8]
        i2c.writeto(HT16K33_ADDR,b'\x00'+bufx)

    def brightness(b):
        i2c = __class__._i2c
        i2c.writeto(HT16K33_ADDR,bytearray([HT16K33_CMD_BRIGHTNESS | (b&0x0f)]))

    # expose framebuf's methods
    pixel = _fb.pixel
    fill = _fb.fill
    hline = _fb.hline
    vline = _fb.vline
    line = _fb.line
    rect = _fb.rect
    fill_rect = _fb.fill_rect
    text = _fb.text
    scroll = _fb.scroll
    blit = _fb.blit

#######################
def init():
    global _ldr,_sw1,_sw2,_i2c1
    matrix.init()
    _ldr = ADC(Pin(LDR_ADC_PIN))
    _ldr.width(ADC.WIDTH_12BIT)
    _ldr.atten(ADC.ATTN_0DB)
    _sw1 = Pin(SW1_PIN, Pin.IN, Pin.PULL_UP)
    _sw2 = Pin(SW2_PIN, Pin.IN, Pin.PULL_UP)
    _i2c1 = I2C(1,scl=Pin(I2C1_SCL),sda=Pin(I2C1_SDA))

#######################
def sw1():
    return _sw1.value() == 0

#######################
def sw2():
    return _sw2.value() == 0

#######################
def light():
    val = min(_ldr.read(), LDR_MAX_VALUE)
    return (LDR_MAX_VALUE - val)*100 // LDR_MAX_VALUE

#######################
def temperature():
    global _i2c1, _lm73_data
    # turn temperature sensor on
    _lm73_data[0] = LM73_REG_CONFIG
    _lm73_data[1] = LM73_POWER_ON
    _i2c1.writeto(LM73_ADDR,_lm73_data)

    # wait at least 40ms then start reading
    time.sleep(0.05)
    _i2c1.writeto(LM73_ADDR,b'\x00')
    _i2c1.readfrom_into(LM73_ADDR,_lm73_data)
    temp = (_lm73_data[0] << 3) | ((_lm73_data[1] >> 5) & 0x07)

    # turn temperature sensor off
    _lm73_data[0] = LM73_REG_CONFIG
    _lm73_data[1] = LM73_POWER_OFF
    _i2c1.writeto(LM73_ADDR,_lm73_data)

    return temp/4
