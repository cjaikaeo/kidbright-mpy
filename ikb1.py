import kidbright

IKB1_I2C_ADDR = const(0x48)

##################################
def init(address=IKB1_I2C_ADDR):
    if not kidbright._i2c1:
        raise Exception("kidbright module not initialized.")
    if address not in kidbright._i2c1.scan():
        raise Exception("Board iKB-1 not connected.")
    kidbright._i2c1.writeto(address,b'\x00')

##################################
def reset(address=IKB1_I2C_ADDR):
    kidbright._i2c1.writeto(address,b'\x00')

##################################
class Input:

    def __init__(self,pin,ikb1_addr=IKB1_I2C_ADDR):
        self.ikb1_addr = ikb1_addr
        self.pin = pin
        self.reg = bytearray([0x08|pin,0x02])

    def value(self):
        kidbright._i2c1.writeto(self.ikb1_addr,self.reg)
        data = kidbright._i2c1.readfrom(IKB1_I2C_ADDR,1)
        return data[0] & 0x01

##################################
class Output:

    def __init__(self,pin,ikb1_addr=IKB1_I2C_ADDR):
        self.ikb1_addr = ikb1_addr
        self.pin = pin
        self.reg = 0x08 | pin
        self.logic = 0

    def value(self,logic=None):
        if logic is None:
            return self.logic
        self.logic = 1 if logic else 0
        logic = self.logic.to_bytes(1,'little')
        kidbright._i2c1.writeto_mem(self.ikb1_addr,self.reg,logic)

##################################
class ADC:

    def __init__(self,pin,ikb1_addr=IKB1_I2C_ADDR):
        self.ikb1_addr = ikb1_addr
        self.pin = pin
        self.reg = 0x80 | (pin<<4)

    def read(self):
        data = kidbright._i2c1.readfrom_mem(self.ikb1_addr,self.reg,2)
        return (data[0] << 8 | data[1]) & 0x3ff

##################################
class Motor:

    def __init__(self,motor_port,ikb1_addr=IKB1_I2C_ADDR):
        self.ikb1_addr = ikb1_addr
        self.port = motor_port
        self.reg = 0x00
        if motor_port == 1: 
            self.reg = 0x21
        elif motor_port == 2:
            self.reg = 0x22
        elif motor_port == 3:
            self.reg = 0x24
        elif motor_port == 4:
            self.reg = 0x28
        else:
            raise Exception("Invalid motor port")
   
    def speed(self,sp):
        sp = sp.to_bytes(1,'little')
        kidbright._i2c1.writeto_mem(self.ikb1_addr,self.reg,sp)
   
    def stop(self):
        self.speed(0)

##################################
class Servo:

    def __init__(self,servo_port,ikb1_addr=IKB1_I2C_ADDR):
        self.ikb1_addr = ikb1_addr
        self.port = servo_port
        self.reg = 0x40
        if servo_port == 10: 
            self.reg |= 0x01
        elif servo_port == 11:
            self.reg |= 0x02
        elif servo_port == 12:
            self.reg |= 0x04
        elif servo_port == 13:
            self.reg |= 0x08
        elif servo_port == 14:
            self.reg |= 0x10
        elif servo_port == 15:
            self.reg |= 0x20
        else:
            raise Exception("Invalid servo port")
  
    def position(self,pos):
        pos = pos.to_bytes(1,'little')
        kidbright._i2c1.writeto_mem(self.ikb1_addr,self.reg,pos)
