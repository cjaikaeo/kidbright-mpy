# The following code demonstrates DC motor control using iKB-1 board.
# Assuming a DC motor is connected to the motor port 1, the code will cause
# the motor to spin at various speeds.
import kidbright as kb
import ikb1
import time

kb.init()
ikb1.init()

m1 = ikb1.Motor(1)

while True:
    for s in [10,50,100]:
        print("Motor spins forward with speed = {}%".format(s))
        m1.speed(s)
        time.sleep(2)
    print("Motor stopped")
    m1.stop()
    time.sleep(2)
    for s in [10,50,100]:
        print("Motor spins backward with speed = {}%".format(s))
        m1.speed(-s)
        time.sleep(2)
    print("Motor stopped")
    m1.stop()
    time.sleep(2)
