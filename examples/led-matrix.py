import time
import kidbright as kb

kb.init()

while True:
    for i in range(100):
        kb.matrix.fill(0)
        kb.matrix.text("{:2d}".format(i), 0, 0)
        kb.matrix.show()
        time.sleep(0.1)
