# The following code demonstrates analog input reading using iKB-1 board.
# Assuming an LDR board is attached to port 2, the code will display the
# analog reading every 0.5 second.
import kidbright as kb
import ikb1
import time

kb.init()
ikb1.init()

ldr = ikb1.ADC(2)

while True:
    print("LDR value =", ldr.read())
    time.sleep(0.5)

